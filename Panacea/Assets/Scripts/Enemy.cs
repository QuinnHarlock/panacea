﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static int count;

    public int health;

    public GameObject win;
    
    public void TakeDamage(int damage)
    {
        health -= damage;
    }
    void Start()
    {
       count = 0;
       win.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            gameObject.SetActive(false);
            count += 1;

        }

        if (count == 10)
        {
            win.SetActive(true);
        }

        
    }


}
