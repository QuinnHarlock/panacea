﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;

    float horizontalMove = 0f;

    public float runSpeed = 40f;
    bool jump = false;
    bool crouch = false;

    bool m_HasGameStarted;
    bool m_HasGameEnded;
    bool m_HasGameDefeated;

    float myTime = 60;
    public Text timer;

    public GameObject defeat;



    private void Start()
    {
        m_HasGameStarted = false;
        m_HasGameEnded = false;
        m_HasGameDefeated = false;
        defeat = GameObject.FindGameObjectWithTag("Defeat");
        defeat.SetActive(false);
    }
    void Update()
    {

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (m_HasGameDefeated == true)
        {
            defeat.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene(0);
            }
        }

        if (myTime <= 0)
        {
            m_HasGameDefeated = true;
        }


        if (m_HasGameStarted == true)
        {
            timer.text = "Time left: " + myTime.ToString("f0");
            myTime -= Time.deltaTime;
        }

            if (m_HasGameEnded == false)
            {
                if (m_HasGameStarted == true)
                {
                    horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

                    if (Input.GetButtonDown("Jump"))
                    {
                        jump = true;
                    }
                    if (Input.GetButtonDown("Crouch"))
                    {
                        crouch = true;
                    } else if (Input.GetButtonUp("Crouch"))
                    {
                        crouch = false;
                    }
                }
            
       
       
            if (Input.GetKeyDown(KeyCode.Return))
                {
                    m_HasGameStarted = true;
                }


            }

        if (Enemy.count >= 10)
        {
            m_HasGameEnded = true;
        }

        if (m_HasGameEnded == true)
        {
            m_HasGameStarted = false;
        }




    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }


}
