﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAim : MonoBehaviour
{
	public Vector3 mousePosition;
	CharacterController2D playerRef;
	public GameObject myPlayer;

	public GameObject projectile;
	public Transform shotPoint;

	private float timeBtwShots;
	public float startTimeBtwShots;

	public float offset;
	bool m_HasGameStarted;

	void Start()
	{
		playerRef = GetComponentInParent<CharacterController2D>();
		m_HasGameStarted = false;
	}

	void Update()
	{
		if (m_HasGameStarted == true)
		{
			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			Vector3 difference = mousePosition - transform.position;
			difference.Normalize();
			float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

			if (playerRef.m_FacingRight)
			{
				transform.rotation = Quaternion.Euler(0f, 0f, rotZ);
			}
			else
			{
				transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 180f);
			}


		

			if (timeBtwShots <= 0)
			{
				if (Input.GetMouseButtonDown(0))
				{
					Instantiate(projectile, shotPoint.position, Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward));
					timeBtwShots = startTimeBtwShots;
				}
			}
			else
			{
				timeBtwShots -= Time.deltaTime;
			}
			
			
		}
		
		if (Input.GetKeyDown(KeyCode.Return))
			{
				m_HasGameStarted = true;
			}

		
	}




}